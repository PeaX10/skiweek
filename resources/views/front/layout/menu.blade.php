<div class="gl-side-menu-overlay"></div>

@if(Auth::check() === false)
<div class="gl-side-menu-wrap">
    <div class="gl-side-menu">
        <div class="gl-side-menu-widget-wrap">
            <div class="gl-login-form-wrapper">
                <h3>Connexion</h3>
                <p>Connectez-vous pour noter une entreprise </p>

                <div class="gl-login-form">
                    @if(Session::has('wrong_password'))
                        <div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Attention!</strong> Mauvais mot de passe !
                        </div>
                    @endif
                    {!! BootForm::open(['method' => 'post', 'url' =>  route('login.post')]) !!}
                        {!! BootForm::text('name', false, false, ['placeholder' => 'Utilisateur', 'id' => 'gl-user-input', 'class' => '']) !!}
                        {!! BootForm::password('password', false, ['placeholder' => 'Mot de passe', 'id' => 'gl-user-password', 'class' => '']) !!}
                        <button type="submit">Connexion</button>
                    {!! BootForm::close() !!}
                </div>

                <div class="gl-social-login-opt">
                    <a href="{{ url('login/facebook') }}" class="gl-social-login-btn gl-facebook-login"><i class="fab fa-facebook-f"></i> Connexion avec Facebook</a>
                </div>

                <div class="gl-other-options">
                    <a href="{{ url('login/forgot') }}" class="gl-forgot-pass">Mot de passe oublié ?</a>
                    <a href="{{ url('login/register') }}" class="gl-signup">Inscription</a>
                </div>
            </div>
        </div>
    </div>

    <button class="gl-side-menu-close-button" id="gl-side-menu-close-button">Fermer Menu</button>
</div>
@endif

<header class="gl-header">
    <div class="gl-header-bottombar">
        <nav class="navbar gl-header-main-menu" role="navigation">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}"><img class="logo" src="{{ url('images/logo-header.png') }}" alt="ETIC"></a>
                </div>

                <div id="nav-menu" class="navbar-collapse gl-menu-wrapper collapse" role="navigation">
                    <ul class="nav navbar-nav gl-menus">
                        <li @if(empty($menu))class="active"@endif>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
                        <li @if(!empty($menu) and $menu == 'about')class="active"@endif>
                            <a href="{{ url('/about') }}">A Propos</a>
                        </li>
                        <li @if(!empty($menu) and $menu == 'inquiry')class="active"@endif>
                            <a href="{{ url('inquiry') }}">Enqueter</a>
                        </li>
                        <li @if(!empty($menu) and $menu == 'blog')class="active"@endif>
                            <a href="{{ url('blog') }}">Blog</a>
                        </li>
                        <li @if(!empty($menu) and $menu == 'contact')class="active"@endif>
                            <a href="{{ url('contact') }}">Contact</a>
                        </li>
                    </ul>
                </div>

                <div class="gl-extra-btns-wrapper">
                    @if(Auth::check())
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownUser">
                            @if(Auth::user()->isAdmin())
                            <li><a href="{{ route('admin.index') }}">Administration</a></li>
                            @endif
                            <li><a href="#">Mon profil</a></li>
                            <li><a href="#">Paramètres</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('login.logout') }}">Déconnexion</a></li>
                        </ul>
                    @else
                        <button class="gl-login-btn" id="gl-side-menu-btn"><i class="fas fa-user"></i> Connexion</button>
                    @endif
                    <button class="gl-add-post-btn" id="gl-side-menu-btn">Noter une entreprise</button>
                </div>
            </div>
        </nav>
    </div>
</header>
@extends('front.layout.default', ['menu' => 'blog', 'bodyClass' => 'gl-blog-page gl-blog-details'])

@section('title', 'Blog - '.$article->title)

@section('css')
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection

@section('content')
    <section class="gl-featured-image-wrapper">
        <picture>
            <img alt="Featured Image" src="{{ $article->getImage() }}">
        </picture>
    </section>

    <section class="gl-page-content-section">
        <div class="container">
            <div class="row">
                <div class="gl-blog-details-wrapper">

                    <div class="gl-blog-heading-metas">
                        <h1 class="gl-blog-titles">{{ $article->title }}</h1>
                        <ul>
                            <li class="gl-blog-post-date">{{ \Carbon\Carbon::make($article->created_at)->format('d M Y') }}</li>
                            <li class="gl-author"><a href="javascript:void(0);">{{ $article->user->name }}</a></li>
                            <li class="gl-comments"><a href="#comments">{{ count($article->comments) }} Commentaires</a></li>
                        </ul>
                    </div>

                    <div class="gl-blog-post-details">
                        {!! $article->content !!}
                    </div>

                    <div class="gl-post-metas">
                        <div class="gl-tag-wrapper">
                            <a href="#" class="gl-tags">tags1</a>
                            <a href="#" class="gl-tags">tags2</a>
                            <a href="#" class="gl-tags">tags3</a>
                        </div>

                        <a data-remodal-target="modal-share" class="gl-btn gl-share-btn btn-success">Partager</a>
                    </div>

                    <div class="gl-post-comments-wrapper" id="comments">
                        @if(count($article->comments) > 0)
                        <h3 class="gl-blog-sec-title">Commentaires</h3>
                            @foreach($article->comments as $comment)
                                @if($comment->user)
                                    <div class="gl-comments">
                                        <div class="gl-user-img">
                                            <img src="@if(!empty($comment->user->avatar)){{ $comment->user->getImage() }}@else{{ url('images/user-img.png') }}@endif" alt="User" class="gl-lazy">
                                        </div>

                                        <div class="gl-comment-text">
                                            <div class="gl-username-date">
                                                <h3>{{ $comment->user->name }}</h3>
                                                <span class="gl-comments-date">{{ \Carbon\Carbon::make($article->created_at)->format('d M Y') }}</span>
                                            </div>
                                            <p>{{ $comment->content }}</p>
                                            <a href="#" class="gl-reply">Répondre</a>
                                        </div>
                                    </div>
                                @else
                                    <div class="gl-comments">
                                        <div class="gl-user-img">
                                            <img src="{{ url('images/user-img.png') }}" alt="User" class="gl-lazy">
                                        </div>

                                        <div class="gl-comment-text">
                                            <div class="gl-username-date">
                                                <h3>{{ $comment->username (invité) }}</h3>
                                                <span class="gl-comments-date">{{ \Carbon\Carbon::make($article->created_at)->format('d M Y') }}</span>
                                            </div>
                                            <p>{{ $comment->content }}</p>
                                            <a href="#" class="gl-reply">Répondre</a>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <p>Il n'y a aucun commentaire pour le moment, soyez le premier !</p>
                        @endif
                    </div>

                    <div class="gl-comments-form-wrapper">
                        {!! BootForm::open() !!}
                        @if(!Auth::check())
                            <fieldset>
                                {!! BootForm::text('username', false, null, ['placeholder' => 'Nom d\'utlisateur']) !!}
                                {!! BootForm::email('email', false, null, ['placeholder' => 'E-mail']) !!}
                                {!! BootForm::text('website', false, null, ['placeholder' => 'Site web']) !!}
                            </fieldset>
                        @endif
                        {!! BootForm::textarea('comment', false, null, ['cols' => 30, 'rows' => 5, 'placeholder' => 'Votre commentaire']) !!}
                        {!! NoCaptcha::renderJs('fr') !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block ">
                                    <strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                        @endif
                            <input type="submit" value="Envoyer" class="gl-btn" style="margin-top:15px">
                        {!! BootForm::close() !!}

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    @if(count($errors) > 0)
        <script type="text/javascript">
            $('#gl-side-menu-btn').trigger('click');
        </script>
    @endif
@endsection
@extends('front.layout.default')

@section('title', 'Réinitialiser mon mot de passe')

@section('content')
    <section class="gl-block-page-header gl-page-header-wrapper">
        <div class="container">
            <div class="row">
                <h1>Réinitialiser le mot de passe</h1>
            </div>
        </div>
    </section>
    <section class="gl-page-content-section" style="padding-top:0px">
        <div class="gl-inner-section">
            <div class="container">
                <div class="row">
                    <div class="gl-contact-form-wrapper col-md-8 col-md-offset-2">
                        <div class="alert alert-info">Merci d'entrer un nouveau mot de passe pour accèder à votre compte</div>
                        {!! BootForm::open() !!}
                        {!! BootForm::password('pass1', false, ['placeholder' => 'Nouveau mot de passe']) !!}
                        {!! BootForm::password('pass2', false, ['placeholder' => 'Répeter le mot de passe']) !!}
                        {!! BootForm::submit('Réinitialiser', ['class' => 'gl-btn pull-right', 'style' => 'margin-top:10px']) !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
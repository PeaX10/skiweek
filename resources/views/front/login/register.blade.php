@extends('front.layout.default')

@section('title', 'Inscription')

@section('content')
    <section class="gl-block-page-header gl-page-header-wrapper">
        <div class="container">
            <div class="row">
                <h1>Inscription</h1>
            </div>
        </div>
    </section>
    <section class="gl-page-content-section" style="padding-top:0px">
        <div class="gl-inner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="gl-sidebar-widget gl-author-widget">
                            <div class="gl-author-img-wrapper" style="width: 150px; height: 150px">
                                <img src="https://picsum.photos/150/150/?random" class="gl-lazy">
                            </div>

                            <div class="gl-author-details">
                                <h3 id="username">Nom d'utilisateur</h3>
                                <p><i class="fas fa-envelope"></i> <span id="email">pzepze@sdf.fr</span></p>

                                <ul class="gl-author-social-links">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-tumblr"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="gl-contact-form-wrapper col-md-8 col-sm-6">
                        {!! BootForm::open(['enctype' => 'multipart/form-data']) !!}
                        {!! BootForm::text('username', false, null, ['placeholder' => 'Nom d\'utilisateur']) !!}
                        {!! BootForm::email('email', false, null, ['placeholder' => 'E-mail']) !!}
                        {!! BootForm::password('pass1', false, ['placeholder' => 'Mot de passe']) !!}
                        {!! BootForm::password('pass2', false, ['placeholder' => 'Confirmer le mot de passe']) !!}
                        <div class="form-group">
                            <div class="input-group input-file" name="avatar">
                                <span class="input-group-btn">
                                    <button class="btn btn-success btn-lg" type="button">Avatar</button>
                                </span>
                                <input type="text" class="form-control" placeholder='Choisir un fichier' />
                            </div>
                        </div>
                        {!! NoCaptcha::renderJs('fr') !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block ">
                                    <strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                        @endif
                        <input type="hidden" value="{{ old('fb_id') }}" name="fb_id">
                        {!! BootForm::submit('Inscription', ['class' => 'gl-btn pull-right', 'style' => 'margin-top:10px']) !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script type="text/javascript">
        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='display:none; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
        $(function() {
            bs_input_file();
            $("input[name='avatar']").change(function () {
                filePreview(this);
            });
        });
        window.setInterval(function(){
            if($('.gl-inner-section input#username').val()){
                $('h3#username').html($('.gl-inner-section input#username').val());
            }else{
                $('h3#username').html("Nom d'utilisateur");
            }

            if($('.gl-inner-section input#email').val()){
                $('p span#email').html($('.gl-inner-section input#email').val());
            }else{
                $('p span#email').html("email@domaine.fr");
            }
        }, 500);

        function filePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.gl-author-img-wrapper img').remove();
                    $('.gl-author-img-wrapper').html('<img src="'+e.target.result+'" width="150" height="150" class="gl-lazy" />');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection
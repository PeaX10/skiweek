@extends('back.layout.default', ['navLink' => 'users'])

@section('title', 'Créer un utilisateur')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer un utilisateur</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer un utilisateur</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer un <code>Utilisateur</code> est très simple, il suffit de renseigner tout les champs du formulaire, à savoir que le champs fb_id sert seulement si l'on possède l'id de l'utilisateur que l'applicaton retourne !
        <hr>
        {!! BootForm::open(['enctype' => 'multipart/form-data', 'url' => route('admin.users.store')]) !!}

            {!! BootForm::text('username', false, null, ['placeholder' => 'Nom d\'utilisateur']) !!}
            {!! BootForm::email('email', false, null, ['placeholder' => 'E-mail']) !!}
            {!! BootForm::password('pass1', false, ['placeholder' => 'Mot de passe']) !!}
            {!! BootForm::password('pass2', false, ['placeholder' => 'Confirmer le mot de passe']) !!}
            {!! BootForm::number('xp', false, null, ['placeholder' => 'Nombre de point d\'XP (numérique)', 'min' => 0]) !!}
            {!! BootForm::select('role', false, [0 => 'Utilisateur', -1 => 'Administrateur']) !!}
            {!! BootForm::text('fb_id', false, null, ['placeholder' => 'ID Facebook']) !!}
            <hr>
            {!! BootForm::file('avatar', 'Avatar', ['placeholder' => 'Avatar']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript">
        $('.form-control-uniform').uniform();
    </script>
@endsection
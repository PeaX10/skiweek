@extends('back.layout.default', ['navLink' => 'blog.categories'])

@section('title', 'Modifier une catégorie')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier une catégorie</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier une catégorie</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier une <code>Catégorie</code> est très simple, il suffit de lui choisir un nom et une icône !
        <hr>
        {!! BootForm::open(['url' => route('admin.blog.categories.store').'/'.$category->id, 'method' => 'PUT', 'model' => $category]) !!}

            {!! BootForm::text('name', false, null, ['placeholder' => 'Nom de la catégorie']) !!}
            <div class="form-group ">
                <select class="form-control select-search" data-fouc name="icon_id">
                    @foreach($icons as $icon)
                        @if(!empty(old('icon_id')))
                            <option value="{{ $icon->id }}" @if($icon->id == old('icon_id')) selected @endif data-icon="{{ $icon->cssClass }}">{{ $icon->cssClass }}</option>
                        @else
                            <option value="{{ $icon->id }}" @if($icon->id == $category->icon_id)) selected @endif data-icon="{{ $icon->cssClass }}">{{ $icon->cssClass }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            function iformat(icon) {
                var originalOption = icon.element;
                return $('<span><i class="' + $(originalOption).data('icon') + '"></i> ' + icon.text + '</span>');
            }
            $('.select-search').select2({
                width: "100%",
                templateSelection: iformat,
                templateResult: iformat,
                allowHtml: true
            });
        });
    </script>
@endsection
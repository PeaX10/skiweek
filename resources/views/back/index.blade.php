@extends('back.layout.default', ['navLink' => 'dashboard'])

@section('title', 'Dashboard')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Dashboard</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <div class="btn-group">
                <button type="button" class="btn bg-indigo-400"><i class="fa fa-check-circle"></i> Valider les notes</button>
                <button type="button" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(161px, 36px, 0px);">
                    <div class="dropdown-header">Nouveau</div>
                    <a href="#" class="dropdown-item"><i class="icon-newspaper"></i> Créer un article</a>
                    <a href="#" class="dropdown-item"><i class="fa fa-industry"></i> Ajouter une entreprise</a>
                    <a href="{{ route('admin.users.create') }}" class="dropdown-item"><i class="fa fa-user"></i> Créer un utilisateur</a>
                    <div class="dropdown-header">Voir</div>
                    <a href="#" class="dropdown-item"><i class="fa fa-newspaper-o"></i> Voir les articles</a>
                    <a href="#" class="dropdown-item"><i class="fa fa-tachometer"></i> Voir les notes</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Bienvenue sur le dashboard</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <h6 class="font-weight-semibold">Pour commencer!</h6>
            <p class="mb-3">Tu trouveras sur la gauche la sidebar qui te permettra de naviguer sur le site, pour l'instant, la majorité n'est pas accessible mais celà arrivera petit à petit!</p>

            <h6 class="font-weight-semibold">Accès rapide?</h6>
            <p class="mb-3">Le petit bouton violet juste au dessus permet de se rendre rapidement vers certaines pages de l'administration, libre à toi de customisez celà avec les raccourcis que tu utiliseras le plus.</p>

            <h6 class="font-weight-semibold">Notification?</h6>
            <p>Et bien why not, si j'ai le temps je ferais un petit système de notification pour ne manquer aucunes tâches.</p>
        </div>
    </div>
@endsection
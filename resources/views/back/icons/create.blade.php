@extends('back.layout.default', ['navLink' => 'icons'])

@section('title', 'Créer une icône')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer une icône</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer une icône</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer une <code>Icône</code> Kest très simple, il suffit de lui choisir une classe CSS !
        <hr>
        {!! BootForm::open(['url' => route('admin.blog.categories.store')]) !!}

            {!! BootForm::text('cssClass', false, null, ['placeholder' => 'Classe CSS de l\'icône']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection
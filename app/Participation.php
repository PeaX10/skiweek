<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{

    const STATES = [0 => 'En cours', 1 => 'Dossier non complet', 2 => 'Dossier complet'];

    const MATOS = [0 => 'Aucun', 1 => 'Ski - Eco', 2 => 'Ski - Découverte', 3 => 'Ski - Sensation', 4 => 'Ski -Excellence', 5 => 'Snow - Eco', 6 => 'Snow - Découverte', 7 => 'Snow - Sensation', 8 => 'Snow -Excellence'];

    const ASSURANCES = [0 => 'Aucune', 1 => 'Annulation', 2 => 'Rapatriment', 3 => 'Interruption Skipass', 4 => 'Tout compris'];

    const FOODPACKS = [0 => 'Aucun', 1 => 'Classic', 2 => 'Hallal', 3 => 'Végétarien'];

    const CLASSES = [0 => 'Aucune', 1 => '3CGP', 2 => '3ETI', 3 => '3IRC', 4 => '4CGP', 5 => '4ETI', 6 => '4IRC',7 => '5CGP', 8 => '5ETI'];

    protected $fillable = ['firstname', 'lastname', 'classe', 'assurance', 'foodpack', 'matos', 'state', 'comment'];

    public function getState(){
        return self::STATES[$this->state];
    }
}

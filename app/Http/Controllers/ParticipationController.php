<?php

namespace App\Http\Controllers;

use App\Participation;
use Illuminate\Http\Request;

class ParticipationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('participations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'firstname' => 'required',
           'lastname' => 'required',
           'classe' => 'required'
        ]);

        $participation = new Participation($request->input());
        if($request->input('prioritaire')) $participation->prioritaire = true;
        $participation->save();

        return redirect()->route('home')->with('participationCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function show(Participation $participation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function edit(Participation $participation)
    {
        return view('participations.edit', compact('participation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participation $participation)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'classe' => 'required'
        ]);

        $participation->update($request->input());
        if($request->input('prioritaire')) $participation->prioritaire = true;
        $participation->save();

        return redirect()->route('home')->with('participationUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participation $participation)
    {
        $participation->delete();
        return redirect()->route('home')->with('participationDeleted', true);
    }
}
